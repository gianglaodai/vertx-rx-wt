package com.hag.vrw.microservice.account.api;

import com.hag.vertx.common.RestAPIRxVerticle;
import com.hag.vrw.microservice.account.Account;
import com.hag.vrw.microservice.account.AccountService;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.ext.web.Router;
import io.vertx.reactivex.ext.web.RoutingContext;
import io.vertx.reactivex.ext.web.handler.BodyHandler;

public class RestUserAccountAPIVerticle extends RestAPIRxVerticle {
    private static final String SERVICE_NAME = "user-account-rest-api";
    private static final Logger LOGGER = LoggerFactory.getLogger(RestUserAccountAPIVerticle.class);

    private final AccountService accountService;

    private static final String API_ADD = "/user";
    private static final String API_RETRIEVE = "/user/:id";
    private static final String API_RETRIEVE_ALL = "/user";
    private static final String API_UPDATE = "/user/:id";
    private static final String API_DELETE = "/user/:id";

    public RestUserAccountAPIVerticle(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        super.start();

        JsonObject config = config();
        final Router router = Router.router(vertx);

        router.route().handler(BodyHandler.create());

        router.post(API_ADD).handler(this::addUser);
        router.get(API_RETRIEVE).handler(this::getUser);
        router.get(API_RETRIEVE_ALL).handler(this::getAll);
        router.patch(API_UPDATE).handler(this::updateUser);
        router.delete(API_DELETE).handler(this::deleteUser);

        String host = config.getString("user.account.http.address", "0.0.0.0");
        int port = config.getInteger("user.account.http.port", 8081);

        createHttpServer(router, host, port).doOnError(e -> LOGGER.error("Error when create Account http server"))
                .flatMap(createdServer -> publishHttpEndpoint(SERVICE_NAME, host, port))
                .doOnSuccess(v -> LOGGER.info("RestUserAccountAPIVerticle service published"))
                .doOnError(e -> LOGGER.error("Error when publish HTTP Endpoint of ResetUserAccountAPIVerticle"))
                .subscribe();
    }

    private void addUser(RoutingContext context) {
        accountService.create(new Account(context.getBodyAsJson()), this.resultVoidHandler(context, 201));
    }

    private void getUser(RoutingContext context) {
        accountService.get(context.request().getParam("id"), resultHandlerNonEmpty(context));
    }

    private void getAll(RoutingContext context) {
        accountService.getAll(resultHandler(context));
    }

    private void updateUser(RoutingContext context) {
        accountService.update(new Account(context.getBodyAsJson()), this.resultHandler(context));
    }

    private void deleteUser(RoutingContext context) {
        accountService.delete(context.request().getParam("id"), deleteResultHandler(context));
    }

}
