package com.hag.vrw.microservice.account;

import com.hag.vertx.common.BaseMicroserviceRxVerticle;
import com.hag.vrw.microservice.account.api.RestUserAccountAPIVerticle;
import com.hag.vrw.microservice.account.impl.AccountServiceImpl;
import io.reactivex.Single;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.config.ConfigRetriever;
import io.vertx.serviceproxy.ServiceBinder;

public class AccountVerticle extends BaseMicroserviceRxVerticle {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountVerticle.class);

    private AccountService accountService;

    @Override
    public void start(Promise<Void> promise) {
        super.start();
        ConfigStoreOptions fileStore = new ConfigStoreOptions().setType("file")
                .setConfig(new JsonObject().put("path", "conf/config.json"));
        ConfigRetriever.create(vertx, new ConfigRetrieverOptions().addStore(fileStore))
                .rxGetConfig()
                .doOnError(e -> LOGGER.error("Can't get configuration", e.getCause()))
                .doOnSuccess(config -> {
                    accountService = new AccountServiceImpl(vertx, config, ar -> {
                        if (ar.succeeded()) {
                            new ServiceBinder(vertx.getDelegate()).setAddress(AccountService.SERVICE_ADDRESS)
                                    .register(AccountService.class, accountService);

                            publishEvenBusService(AccountService.SERVICE_NAME,
                                    AccountService.SERVICE_ADDRESS,
                                    AccountService.class).doOnError(e -> LOGGER.error(
                                    "Error when publish Account Service Even Bus"))
                                    .flatMap(v -> this.deployRestVerticle(config))
                                    .doOnSuccess(s -> {
                                        LOGGER.info("AccountService is published");
                                        promise.complete();
                                    })
                                    .subscribe();
                            return;
                        }
                        LOGGER.error("AccountService fail", ar.cause());
                        promise.fail(ar.cause());
                    });
                }).subscribe();

    }

    private Single<String> deployRestVerticle(JsonObject config) {
        return vertx.rxDeployVerticle(new RestUserAccountAPIVerticle(accountService),
                new DeploymentOptions().setConfig(config))
                .doOnSuccess(s -> LOGGER.info("AccountVerticle is deployed"))
                .doOnError(e -> LOGGER.error("Error when deploy " + "RextUserAccountAPIVerticle", e.getCause()));
    }
}

