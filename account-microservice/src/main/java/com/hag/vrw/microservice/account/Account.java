package com.hag.vrw.microservice.account;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.*;
import lombok.experimental.Accessors;

@DataObject(generateConverter = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Data
@Getter
@Setter
@Accessors(chain = true)
public class Account {
    private String id;
    private String username;
    private String phone;
    private String email;
    private Long birthDate;
    private Long createdDate;
    private Long updatedAt;

    public Account(JsonObject json) {
        AccountConverter.fromJson(json, this);
    }

    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        AccountConverter.toJson(this, json);
        return json;
    }

    public JsonArray toJsonArray() {
        return new JsonArray().add(id).add(username).add(phone).add(email).add(birthDate).add(createdDate).add(updatedAt);
    }


}
