package com.hag.vrw.microservice.account;

import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

import java.util.List;

@VertxGen
@ProxyGen
public interface AccountService {
    public static final String SERVICE_NAME = "account-eb-service";

    public static final String SERVICE_ADDRESS = "vrw.service.account";

    @Fluent
    AccountService initializePersistence(Handler<AsyncResult<Void>> resultHandler);

    @Fluent
    AccountService create(Account account, Handler<AsyncResult<Void>> resultHandler);

    @Fluent
    AccountService get(String id, Handler<AsyncResult<Account>> resultHandler);

    @Fluent
    AccountService getByUsername(String username, Handler<AsyncResult<Account>> resultHandler);

    @Fluent
    AccountService getAll(Handler<AsyncResult<List<Account>>> resultHandler);

    @Fluent
    AccountService update(Account account, Handler<AsyncResult<Account>> resultHandler);

    @Fluent
    AccountService delete(String id, Handler<AsyncResult<Void>> resultHandler);

    @Fluent
    AccountService deleteAll(Handler<AsyncResult<Void>> resultHandler);
}
