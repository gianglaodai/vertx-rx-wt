package com.hag.vrw.microservice.account.impl;

import com.hag.vertx.common.service.JdbcRepositoryWrapper;
import com.hag.vrw.microservice.account.Account;
import com.hag.vrw.microservice.account.AccountService;
import io.reactivex.Flowable;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.ext.sql.SQLConnection;

import java.util.List;
import java.util.stream.Collectors;

public class AccountServiceImpl extends JdbcRepositoryWrapper implements AccountService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountServiceImpl.class);

    public AccountServiceImpl(Vertx vertx, JsonObject config, Handler<AsyncResult<Void>> initHandler) {
        super(vertx, config);
        this.initializePersistence(initHandler);
    }

    @Override
    public AccountService initializePersistence(Handler<AsyncResult<Void>> resultHandler) {
        this.getClient()
                .rxGetConnection()
                .doAfterSuccess(SQLConnection::close)
                .flatMap(conn -> conn.rxExecute(CREATE_STATEMENT).toSingleDefault(true))
                .doOnSuccess(r -> resultHandler.handle(Future.succeededFuture()))
                .doOnError(this.errorHandler(resultHandler))
                .subscribe();
        return this;
    }

    @Override
    public AccountService create(Account account, Handler<AsyncResult<Void>> resultHandler) {
        this.executeNoResult(INSERT_STATEMENT, account.toJsonArray())
                .map(Future::succeededFuture)
                .doOnSuccess(resultHandler::handle)
                .doOnError(this.errorHandler(resultHandler))
                .subscribe();
        return this;
    }

    @Override
    public AccountService get(String id, Handler<AsyncResult<Account>> resultHandler) {
        this.getOne(FETCH_STATEMENT, id)
                .map(Account::new)
                .map(Future::succeededFuture)
                .doOnSuccess(resultHandler::handle)
                .doOnError(this.errorHandler(resultHandler))
                .subscribe();
        return this;
    }

    @Override
    public AccountService getByUsername(String username, Handler<AsyncResult<Account>> resultHandler) {
        this.getOne(FETCH_BY_USERNAME_STATEMENT, username)
                .map(Account::new)
                .map(Future::succeededFuture)
                .doOnSuccess(resultHandler::handle)
                .doOnError(this.errorHandler(resultHandler))
                .subscribe();
        return this;
    }

    @Override
    public AccountService getAll(Handler<AsyncResult<List<Account>>> resultHandler) {
        this.getAll(FETCH_ALL_STATEMENT)
                .flatMap(arr -> Flowable.fromIterable(arr.stream()
                        .map(o -> (JsonObject) o)
                        .map(Account::new)
                        .collect(Collectors.toList())))
                .toList()
                .map(Future::succeededFuture)
                .doOnSuccess(resultHandler::handle)
                .doOnError(this.errorHandler(resultHandler))
                .subscribe();
        return this;
    }

    @Override
    public AccountService update(Account account, Handler<AsyncResult<Account>> resultHandler) {
        this.execute(UPDATE_STATEMENT,
                new JsonArray().add(account.getUsername())
                        .add(account.getPhone())
                        .add(account.getEmail())
                        .add(account.getBirthDate())
                        .add(account.getUpdatedAt())
                        .add(account.getId()),
                account)
                .map(Future::succeededFuture)
                .doOnSuccess(resultHandler::handle)
                .doOnError(this.errorHandler(resultHandler))
                .subscribe();
        return this;
    }

    @Override
    public AccountService delete(String id, Handler<AsyncResult<Void>> resultHandler) {
        this.remove(DELETE_STATEMENT, id)
                .map(Future::succeededFuture)
                .doOnSuccess(resultHandler::handle)
                .doOnError(this.errorHandler(resultHandler))
                .subscribe();
        return this;
    }

    @Override
    public AccountService deleteAll(Handler<AsyncResult<Void>> resultHandler) {
        this.removeAll(DELETE_ALL_STATEMENT)
                .map(Future::succeededFuture)
                .doOnSuccess(resultHandler::handle)
                .doOnError(this.errorHandler(resultHandler))
                .subscribe();
        return this;
    }

    private static final String CREATE_STATEMENT = "CREATE TABLE IF NOT EXISTS `user_account` (`id` varchar(30) NOT NULL,`username` varchar(20) NOT NULL,`phone` varchar(20) NOT NULL,`email` varchar(45) NOT NULL,`birthDate` bigint(20) NOT NULL, `createdDate` bigint(20) NOT NULL, `updatedAt` bigint(20) NOT NULL,PRIMARY KEY (`id`),UNIQUE KEY `username_UNIQUE` (`username`) )";
    private static final String INSERT_STATEMENT = "INSERT INTO user_account (id, username, phone, email, birthDate, createdDate, updatedAt) VALUES (?, ?, ?, ?, ?, ?, ?)";
    private static final String FETCH_STATEMENT = "SELECT * FROM user_account WHERE id = ?";
    private static final String FETCH_BY_USERNAME_STATEMENT = "SELECT * FROM user_account WHERE username = ?";
    private static final String FETCH_ALL_STATEMENT = "SELECT * FROM user_account";
    private static final String UPDATE_STATEMENT = "UPDATE `user_account` SET `username` = ?,`phone` = ?,`email` = ?,`birthDate` = ?,`updatedAt` = ?  WHERE `id` = ?";
    private static final String DELETE_STATEMENT = "DELETE FROM user_account WHERE id = ?";
    private static final String DELETE_ALL_STATEMENT = "DELETE FROM user_account";
}
