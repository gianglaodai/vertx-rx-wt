package com.hag.vertx.common;

import com.hag.vertx.common.enums.HttpStatusCode;
import io.reactivex.Single;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.http.HttpServer;
import io.vertx.reactivex.ext.web.Router;
import io.vertx.reactivex.ext.web.RoutingContext;
import io.vertx.reactivex.ext.web.handler.CookieHandler;
import io.vertx.reactivex.ext.web.handler.CorsHandler;
import io.vertx.reactivex.ext.web.handler.SessionHandler;
import io.vertx.reactivex.ext.web.sstore.ClusteredSessionStore;
import io.vertx.reactivex.ext.web.sstore.LocalSessionStore;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;

import static com.hag.vertx.common.enums.HttpStatusCode.*;

public abstract class RestAPIRxVerticle extends BaseMicroserviceRxVerticle {
    public static final CharSequence APPLICATION_JSON = HttpHeaders.createOptimized("application/json");
    public static final String ERROR = "error";
    public static final String MESSAGE = "message";

    protected Single<HttpServer> createHttpServer(Router router, String host, int port) {
        return vertx.createHttpServer().requestHandler(router).rxListen(port, host);
    }

    protected void enableCorsSupport(Router router) {
        router.route()
                .handler(CorsHandler.create("*")
                        .allowedHeaders(Set.of("x-requested-with",
                                "Access-Control-Allow-Origin",
                                "origin",
                                "Content-Type",
                                "accept"))
                        .allowedMethods(Set.of(HttpMethod.GET,
                                HttpMethod.POST,
                                HttpMethod.PUT,
                                HttpMethod.DELETE,
                                HttpMethod.PATCH,
                                HttpMethod.OPTIONS)));
    }

    protected void enableLocalSession(Router router, String name) {
        Objects.requireNonNull(name);
        router.route().handler(CookieHandler.create());
        router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx, name)));
    }

    protected void enableClusteredSession(Router router, String name) {
        Objects.requireNonNull(name);
        router.route().handler(CookieHandler.create());
        router.route().handler(SessionHandler.create(ClusteredSessionStore.create(vertx, name)));
    }

    protected void requireLogin(RoutingContext context, BiConsumer<RoutingContext, JsonObject> biConsumer) {
        Optional.ofNullable(context.request().getHeader("user-principal"))
                .map(JsonObject::new)
                .ifPresentOrElse(pricipal -> biConsumer.accept(context, pricipal),
                        () -> this.errorResponse(context, UNAUTHORIZED, new JsonObject().put(MESSAGE, "need auth")));
    }

    protected <T> Handler<AsyncResult<T>> resultHandler(RoutingContext context) {
        return ar -> {
            if (ar.succeeded()) {
                T res = ar.result();
                context.response()
                        .putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON)
                        .end(this.resultData(res).encodePrettily());
            } else {
                internalServerError(context, ar.cause());
                ar.cause().printStackTrace();
            }
        };
    }

    protected <T> Handler<AsyncResult<T>> resultHandler(RoutingContext context, Function<T, String> converter) {
        return ar -> {
            if (ar.succeeded()) {
                T res = ar.result();
                if (res == null) {
                    serviceUnavailable(context, new JsonObject().put(MESSAGE, "invalid_result"));
                } else {
                    context.response()
                            .putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON)
                            .end(this.resultData(converter.apply(res)).encodePrettily());
                }
            } else {
                internalServerError(context, ar.cause());
                ar.cause().printStackTrace();
            }
        };
    }

    protected <T> Handler<AsyncResult<T>> resultHandlerNonEmpty(RoutingContext context) {
        return ar -> {
            if (ar.succeeded()) {
                T res = ar.result();
                if (res == null) {
                    notFound(context);
                } else {
                    context.response()
                            .putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON)
                            .end(this.resultData(res).encodePrettily());
                }
            } else {
                internalServerError(context, ar.cause());
                ar.cause().printStackTrace();
            }
        };
    }

    protected Handler<AsyncResult<Void>> resultVoidHandler(RoutingContext context, int status) {
        return ar -> {
            if (ar.succeeded()) {
                context.response()
                        .setStatusCode(status == 0 ? 200 : status)
                        .putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON)
                        .end();
            } else {
                internalServerError(context, ar.cause());
                ar.cause().printStackTrace();
            }
        };
    }

    protected Handler<AsyncResult<Void>> deleteResultHandler(RoutingContext context) {
        return res -> {
            if (res.succeeded()) {
                context.response()
                        .setStatusCode(204)
                        .putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON)
                        .end(resultData(new JsonObject().put(MESSAGE, "delete_success")).encodePrettily());
            } else {
                internalServerError(context, res.cause());
                res.cause().printStackTrace();
            }
        };
    }

    protected Handler<AsyncResult<Void>> resultVoidHandler(RoutingContext context, JsonObject result, int status) {
        return ar -> {
            if (ar.succeeded()) {
                context.response()
                        .setStatusCode(status == 0 ? 200 : status)
                        .putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON)
                        .end(this.resultData(result).encodePrettily());
            } else {
                internalServerError(context, ar.cause());
                ar.cause().printStackTrace();
            }
        };
    }

    protected Handler<AsyncResult<Void>> resultVoidHandler(RoutingContext context, JsonObject result) {
        return resultVoidHandler(context, result, 200);
    }

    private JsonObject resultData(Object result) {
        return new JsonObject().put("data", result);
    }

    protected void badRequest(RoutingContext context, JsonObject cause) {
        errorResponse(context, BAD_REQUEST, cause);
    }

    protected void notFound(RoutingContext context) {
        errorResponse(context, NOT_FOUND, new JsonObject().put(MESSAGE, "not found"));
    }

    protected void internalServerError(RoutingContext context, Throwable ex) {
        errorResponse(context, INTERNAL_SERVER_ERROR, new JsonObject().put(MESSAGE, ex.getMessage()));
    }

    protected void notImplemented(RoutingContext context) {
        errorResponse(context, NOT_IMPLEMENTED, new JsonObject().put(MESSAGE, "not implemented"));
    }

    protected void badGateway(RoutingContext context, Throwable ex) {
        ex.printStackTrace();
        errorResponse(context, BAD_GATEWAY, new JsonObject().put(MESSAGE, "bad gateway"));
    }

    protected void serviceUnavailable(RoutingContext context, JsonObject cause) {
        errorResponse(context, SERVICE_UNAVAILABLE, cause);
    }


    protected void errorResponse(RoutingContext context, HttpStatusCode code, JsonObject cause) {
        context.response()
                .setStatusCode(code.getCode())
                .putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON)
                .end(new JsonObject().put(ERROR, cause).encodePrettily());
    }
}
