package com.hag.vertx.common.service;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.ext.jdbc.JDBCClient;
import io.vertx.reactivex.ext.sql.SQLConnection;
import io.vertx.reactivex.ext.sql.SQLRowStream;

public class JdbcRepositoryWrapper {
    private final JDBCClient client;

    protected JDBCClient getClient() {
        return this.client;
    }


    public JdbcRepositoryWrapper(Vertx vertx, JsonObject config) {
        this.client = JDBCClient.createNonShared(vertx, config);
    }

    protected Single<Void> executeNoResult(String sql, JsonArray params) {
        return client
                .rxGetConnection()
                .doAfterSuccess(SQLConnection::close)
                .flatMap(conn -> conn.rxUpdateWithParams(sql, params))
                .map(result -> null);
    }

    protected <R> Single<R> execute(String sql, JsonArray params, final R ret) {
        return client.rxGetConnection()
                .doAfterSuccess(SQLConnection::close)
                .flatMap(conn -> conn.rxUpdateWithParams(sql, params))
                .map(result -> ret);
    }

    protected <K> Maybe<JsonObject> getOne(String sql, K param) {
        return client.rxGetConnection()
                .doAfterSuccess(SQLConnection::close)
                .toMaybe()
                .flatMap(connection -> connection.rxQuerySingleWithParams(sql, new JsonArray().add(param))
                        .map(jsonArr -> jsonArr.getJsonObject(0)));
    }

    protected Flowable<JsonArray> getByPage(String sql, int page, int limit) {
        JsonArray params = new JsonArray().add(calcPage(page, limit)).add(limit);
        return getMany(sql, params);
    }

    protected Flowable<JsonArray> getMany(String sql, JsonArray params) {
        return client.rxGetConnection()
                .doAfterSuccess(SQLConnection::close)
                .flatMap(conn -> conn.rxQueryStreamWithParams(sql, params))
                .doAfterSuccess(SQLRowStream::close)
                .toFlowable()
                .flatMap(SQLRowStream::toFlowable);
    }

    protected Flowable<JsonArray> getAll(String sql) {
        return client.rxGetConnection()
                .doAfterSuccess(SQLConnection::close)
                .flatMap(conn -> conn.rxQueryStream(sql))
                .doAfterSuccess(SQLRowStream::close)
                .toFlowable()
                .flatMap(SQLRowStream::toFlowable);
    }

    protected <K> Single<Void> remove(String sql, K id) {
        return client.rxGetConnection().doAfterSuccess(SQLConnection::close)
                .flatMap(conn -> conn.rxUpdateWithParams(sql, new JsonArray().add(id)))
                .map(result -> null);
    }

    protected Single<Void> removeAll(String sql) {
        return client.rxGetConnection().doAfterSuccess(SQLConnection::close)
                .flatMap(conn -> conn.rxUpdate(sql))
                .map(resutl -> null);
    }

    protected int calcPage(int page, int limit) {
        return page <= 0 ? 0 : limit * (page - 1);
    }

    protected <R> Consumer<Throwable> errorHandler(Handler<AsyncResult<R>> resultHandler) {
        return e -> resultHandler.handle(Future.failedFuture(e.getCause()));
    }

    private Consumer<Void> successHandlerWithoutResult(Handler<AsyncResult<Void>> resultHandler) {
        return r -> resultHandler.handle(Future.succeededFuture());
    }

}
