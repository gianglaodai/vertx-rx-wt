package com.hag.vertx.common;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.Promise;
import io.vertx.core.impl.ConcurrentHashSet;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.circuitbreaker.CircuitBreaker;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.servicediscovery.ServiceDiscovery;
import io.vertx.reactivex.servicediscovery.types.HttpEndpoint;
import io.vertx.reactivex.servicediscovery.types.JDBCDataSource;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscoveryOptions;
import io.vertx.servicediscovery.types.EventBusService;
import io.vertx.servicediscovery.types.MessageSource;

import java.util.Optional;
import java.util.Set;

public abstract class BaseMicroserviceRxVerticle extends AbstractVerticle {
    private static final Logger logger = LoggerFactory.getLogger(BaseMicroserviceRxVerticle.class);

    private ServiceDiscovery discovery;

    private CircuitBreaker circuitBreaker;

    private Set<Record> registeredRecords = new ConcurrentHashSet<>();

    protected ServiceDiscovery getDiscovery() {
        return discovery;
    }

    protected CircuitBreaker getCircuitBreaker() {
        return circuitBreaker;
    }

    protected Set<Record> getRegisteredRecords() {
        return registeredRecords;
    }

    @Override
    public void start() {
        discovery = ServiceDiscovery.create(vertx, new ServiceDiscoveryOptions().setBackendConfiguration(config()));

        JsonObject circuitBreakerOptions = Optional.ofNullable(config().getJsonObject("circuit-breaker"))
                .orElse(new JsonObject());

        circuitBreaker = CircuitBreaker.create(circuitBreakerOptions.getString("name", "circuit-breaker"),
                vertx,
                new CircuitBreakerOptions().setMaxFailures(circuitBreakerOptions.getInteger("maxFailures", 5))
                        .setTimeout(circuitBreakerOptions.getLong("timeout", 10000L))
                        .setFallbackOnFailure(true)
                        .setResetTimeout(circuitBreakerOptions.getLong("resetTimeout", 30000L)));
    }

    protected Single<Record> publishHttpEndpoint(String name, String host, int port) {
        Record record = HttpEndpoint.createRecord(name,
                host,
                port,
                "/",
                new JsonObject().put("api.name", config().getString("api.name", "")));
        return publish(record);
    }

    protected Single<Record> publishApiGateway(String host, int port) {
        Record record = HttpEndpoint.createRecord("api-gateway", true, host, port, "/", null).setType("api-gateway");
        return publish(record);
    }

    protected Single<Record> publishMessageSource(String name, String address) {
        Record record = MessageSource.createRecord(name, address);
        return publish(record);
    }

    protected Single<Record> publishJDBCDataSource(String name, JsonObject location) {
        Record record = JDBCDataSource.createRecord(name, location, new JsonObject());
        return publish(record);
    }

    protected Single<Record> publishEvenBusService(String name, String address, Class serviceClass) {
        Record record = EventBusService.createRecord(name, address, serviceClass);
        return publish(record);
    }

    private Single<Record> publish(Record record) {
        return discovery.rxPublish(record).doOnSuccess(rec -> {
            registeredRecords.add(record);
            logger.info("Service <" + rec.getName() + "> published");
        }).doOnError(e -> logger.error("Can't publish record " + record.getName()));
    }

    @Override
    public void stop(Promise<Void> stopPromise) throws Exception {
        Flowable.fromIterable(registeredRecords)
                .flatMap(record -> discovery.rxUnpublish(record.getRegistration()).toFlowable())
                .reduce((Void) null, (a, b) -> null)
                .subscribe(stopPromise::complete, stopPromise::fail);
    }
}
