package com.hag.vrw.gateway;

import com.hag.vertx.common.RestAPIRxVerticle;
import com.hag.vrw.microservice.account.Account;
import com.hag.vrw.microservice.account.AccountService;
import io.reactivex.Single;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.oauth2.OAuth2FlowType;
import io.vertx.reactivex.config.ConfigRetriever;
import io.vertx.reactivex.core.buffer.Buffer;
import io.vertx.reactivex.core.http.HttpServerResponse;
import io.vertx.reactivex.ext.auth.User;
import io.vertx.reactivex.ext.auth.oauth2.KeycloakHelper;
import io.vertx.reactivex.ext.auth.oauth2.OAuth2Auth;
import io.vertx.reactivex.ext.auth.oauth2.providers.KeycloakAuth;
import io.vertx.reactivex.ext.web.Router;
import io.vertx.reactivex.ext.web.RoutingContext;
import io.vertx.reactivex.ext.web.client.HttpRequest;
import io.vertx.reactivex.ext.web.client.WebClient;
import io.vertx.reactivex.ext.web.handler.BodyHandler;
import io.vertx.reactivex.ext.web.handler.SessionHandler;
import io.vertx.reactivex.ext.web.handler.StaticHandler;
import io.vertx.reactivex.ext.web.sstore.LocalSessionStore;
import io.vertx.reactivex.servicediscovery.types.HttpEndpoint;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.types.EventBusService;

import java.util.List;
import java.util.Optional;

public class APIGatewayVerticle extends RestAPIRxVerticle {
    private static final int DEFAULT_PORT = 8787;

    private static final Logger LOGGER = LoggerFactory.getLogger(APIGatewayVerticle.class);

    private OAuth2Auth oAuth2;

    @Override
    public void start(Promise<Void> startPromise) {
        super.start();
        ConfigStoreOptions fileStore = new ConfigStoreOptions().setType("file")
                .setConfig(new JsonObject().put("path", "conf/config.json"));
        ConfigRetriever.create(vertx, new ConfigRetrieverOptions().addStore(fileStore))
                .rxGetConfig()
                .doOnError(e -> LOGGER.error("Can't get configuration", e.getCause()))
                .doOnSuccess(config -> {
                    String host = config.getString("api.gateway.http.address", "localhost");
                    int port = config.getInteger("api.gateway.http.port", DEFAULT_PORT);

                    Router router = Router.router(vertx);

                    enableLocalSession(router, "vrw.user.session");

                    router.route().handler(BodyHandler.create());

                    router.get("/api/v").handler(this::apiVersion);

                    oAuth2 = KeycloakAuth.create(vertx, OAuth2FlowType.AUTH_CODE, config);

                    router.route()
                            .handler(SessionHandler.create(LocalSessionStore.create(vertx)).setAuthProvider(oAuth2));

                    String hostURI = String.format("https://%s:%d", host, port);

                    router.route("/callback").handler(context -> authCallback(oAuth2, hostURI, context));

                    router.get("/uaa").handler(this::authUaaHandler);

                    router.get("/login").handler(this::loginEntryHandler);

                    router.post("/logout").handler(this::logoutHandler);

                    router.route("/api/*").handler(this::dispatchRequests);

                    router.route("/*").handler(StaticHandler.create());

                    vertx.createHttpServer()
                            .requestHandler(router::accept)
                            .rxListen(port, host)
                            .doOnSuccess(v -> LOGGER.info("API gate way is running on port " + port))
                            .doOnError(e -> LOGGER.error("Can't start api gate way", e.getCause()))
                            .flatMap(server -> publishApiGateway(host, port))
                            .subscribe();
                })
                .subscribe();
    }

    private void apiVersion(RoutingContext context) {
        context.response().end(new JsonObject().put("version", "v1").encodePrettily());
    }

    private void authCallback(OAuth2Auth oAuth2, String hostURI, RoutingContext context) {
        String redirectURI = hostURI + context.currentRoute().getPath() + "?redirect_uri=" + context.request()
                .getParam("redirect_uri");
        Optional.ofNullable(context.request().getParam("code"))
                .map(c -> new JsonObject().put("code", c).put("redirect_uri", redirectURI))
                .ifPresentOrElse(json -> {
                    oAuth2.authenticate(json, ar -> {
                        if (ar.failed()) {
                            LOGGER.warn("Auth fail");
                            context.fail(ar.cause());
                        } else {
                            LOGGER.info("Auth success");
                            context.setUser(ar.result());
                            context.response()
                                    .putHeader("Location", json.getString("redirect_uri"))
                                    .setStatusCode(302)
                                    .end();
                        }
                    });
                }, () -> {
                    context.fail(400);
                });
    }

    private void authUaaHandler(RoutingContext context) {
        Optional.ofNullable(context.user())
                .map(User::principal)
                .map(KeycloakHelper::preferredUsername)
                .ifPresentOrElse(username -> {
                    Promise<AccountService> promise = Promise.promise();
                    EventBusService.getProxy(this.getDiscovery().getDelegate(), AccountService.class, promise);
                    promise.future()
                            .compose(accountService -> {
                                Promise<Account> accountPromise = Promise.promise();
                                accountService.getByUsername(username, accountPromise);
                                return accountPromise.future().map(acc -> {
                                    ServiceDiscovery.releaseServiceObject(this.getDiscovery().getDelegate(),
                                            accountService);
                                    return acc;
                                });
                            })
                            .setHandler(this.resultHandlerNonEmpty(context)); //TODO - if user does not exist, should return 404
                }, () -> context.fail(401));
    }

    private void loginEntryHandler(RoutingContext context) {
        context.response().putHeader("Location", generateAuthRedirectURI(buildHostURI())).setStatusCode(302).end();
    }

    private String generateAuthRedirectURI(String from) {
        return oAuth2.authorizeURL(new JsonObject().put("redirect_uri", from + "/callback?redirect_uri=" + from)
                .put("scope", "")
                .put("state", ""));
    }

    private String buildHostURI() {
        int port = config().getInteger("api.gateway.http.port", DEFAULT_PORT);
        final String host = config().getString("api.gateway.http.address.external", "localhost");
        return String.format("https://%s:%d", host, port);
    }

    private void logoutHandler(RoutingContext context) {
        context.clearUser();
        context.session().destroy();
        context.response().setStatusCode(204).end();
    }

    private void dispatchRequests(RoutingContext context) {
        int initialOffset = 5; // length of `/api/`
        getCircuitBreaker().execute(promise -> {
            getAllEndPoints().doOnSuccess(records -> {
                String path = context.request().uri();
                if (path.length() <= initialOffset) {
                    notFound(context);
                    promise.complete();
                    return;
                }
                String prefix = path.substring(initialOffset).split("/")[0];
                String newPath = path.substring(initialOffset + prefix.length());

                records.stream()
                        .filter(record -> record.getMetadata().getString("api.name") != null)
                        .filter(record -> record.getMetadata().getString("api.name").equals(prefix))
                        .findAny()
                        .ifPresentOrElse(record -> doDispatch(context,
                                newPath,
                                getDiscovery().getReference(record).get(),
                                promise), () -> {
                            notFound(context);
                            promise.complete();
                        });
            }).doOnError(e -> promise.fail(e.getCause()));
        }).setHandler(ar -> {
            if (ar.failed()) {
                badGateway(context, ar.cause());
            }
        });

    }

    private Single<List<Record>> getAllEndPoints() {
        return this.getDiscovery().rxGetRecords(record -> record.getType().equals(HttpEndpoint.TYPE));
    }

    private void doDispatch(RoutingContext context,
                            String path,
                            WebClient client,
                            io.vertx.reactivex.core.Promise<Object> cbPromise) {
        HttpRequest<Buffer> httpRequest = client.request(context.request().method(), path);

        context.request().headers().forEach(header -> {
            httpRequest.putHeader(header.getKey(), header.getValue());
        });
        Optional.ofNullable(context.user())
                .map(User::principal)
                .map(JsonObject::encode)
                .ifPresent(userPrincipal -> httpRequest.putHeader("user-principal", userPrincipal));
        httpRequest.rxSendJsonObject(context.getBodyAsJson()).doOnSuccess(response -> {
            if (response.statusCode() >= 500) {
                cbPromise.fail(response.statusCode() + ": " + response.body().toString());
            } else {
                HttpServerResponse toRsp = context.response().setStatusCode(response.statusCode());
                response.headers().forEach(header -> toRsp.putHeader(header.getKey(), header.getValue()));
                toRsp.end(response.body());
                cbPromise.complete();
            }
            ServiceDiscovery.releaseServiceObject(this.getDiscovery().getDelegate(), client);
        }).doOnError(e -> cbPromise.fail(e.getCause())).subscribe();
    }
}
